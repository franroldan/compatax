#!/usr/bin/perl
use strict;
use warnings;

##########
## Francisco Javier Roldán Alés
## TFM - Comparador de resultados obtenidos con Sma3s sobre
## organismos modelo almacenados en una base de datos MongoDB
##########
# Ejemplo: subida de nuevo organismo modelo a la base de datos
# compatax.pl -option upload -file summary.tsv -name Ecoli -source Uniprot -taxid 34634634 -tags Bacteria,Coli
##########
# Ejemplo: subida múltiple de varios organismos modelo a la base de datos
# compatax.pl -option uploadmultiple -file tabla.csv
##########
# Ejemplo: listar organismos modelo almacenados en base de datos
# compatax.pl -option list
##########
# Ejemplo: comparar un fichero frente a un organismo modelo almacenado
# compatax.pl -option compare -file summary.tsv -taxid ASD123
##########
# Ejemplo: buscar los 10 organismos modelo más similares al fichero
# compatax.pl -option find -file summary.tsv -num 10 -tags Bacteria,Coli
##########

##############################
##############################

# Rutina que se ejecuta al principio del programa:
# comprueba la existencia de los módulos necesarios
BEGIN {
	# Comprobación de módulos requeridos
	my $errors = 0;
	my @imports = qw(Data::Dumper Getopt::Long MongoDB Path::Tiny DateTime JSON::MaybeXS File::Copy Storable File::Basename Cwd Timer::Runtime);
    for my $imp (@imports) {
        unless( eval "require $imp" ) {
			warn "Error: Module $imp is required\n";
			++$errors;
        } else {
			$imp->import();
		}
    }
	use threads;
	use threads::shared;
}

# Definición de constantes para la conexión con MongoDB
use constant MONGODB_HOST => '127.0.0.1';
use constant MONGODB_PORT => 27017;
use constant MONGODB_DATABASE => 'Compatax';
use constant MONGODB_COLLECTION => 'Sma3s-models';

# Constante para controlar el mínimo de anotaciones a tener en cuenta
use constant MIN_ANNOTATIONS => 3;

# Constante para controlar el número de organismos a usar en la opción
# 'find' si no se especifica el parámetro 'num'
use constant NUM_FIND_ORGANISM => 5;

# Constante para controlar el número máximo de hilos que se pueden ejecutar
# de forma paralela
use constant MAX_THREADS => 20;

# Constantes para el nombre de los ficheros de resultados
use constant COMPARE_SUMARY_FILE => 'compare.html';
use constant FIND_SUMARY_FILE => 'find.html';

# Definición de variables
my $option = '';
my $tsv_file = '';
my $name = '';
my $tax_id = '';
my $source = '';
my $tags = '';
my $json_distances = '';
my $num_distances = 0;
my $error = 0;
my $summary;
my $cursor;
my $document;
my $distance;
my $num_genes;
my $num_hilos;
my $json;
my $replace;
my $path;
my $data;
my $error_file;
my @distances = ();
my @tags_array;
my $print_help;

# Variables compartidas por hilos
my @shared_distances :shared = ();

# Leemos los parámetros de entrada
GetOptions(
	"option=s" => \$option,
	"file=s" => \$tsv_file,
	"name=s" => \$name,
	"taxid=s" => \$tax_id,
	"source=s" => \$source,
	"tags=s" => \$tags,
	"num=i" => \$num_distances,
	"help|h" => \$print_help
);
if($print_help){
	help();
	exit;
}
if(length $option == 0) {
	print "Error: param -option required\n";
	exit;
}

# Conexión con MongoDB
my ($mongodb_connection, $mongodb) = mongoConnect();

# Gestionamos la opción correspondiente
if($option eq 'uploadmultiple') {
    ##########
    # Subida de varios organismos
    ##########
    # Comprobamos los parámetros
    $error_file = existsFile($tsv_file);
    if(length $error_file > 0){
        print $error_file;
    } else {
        readCSVTable($tsv_file);
    }

} elsif($option eq 'upload') {
	##########
	# Subida de un nuevo organismo modelo a la BD
	##########
	# Comprobamos los parámetros
	if(length $name == 0){
		print "Error: param -name required\n";
		$error = 1;
	}
	if(length $tax_id == 0){
		print "Error: param -taxid required\n";
		$error = 1;
	}
	if(length $source == 0){
		print "Error: param -source required\n";
		$error = 1;
	}
	$error_file = existsFile($tsv_file);
	if(length $error_file > 0){
		print $error_file;
		$error = 1;
	}
	# Si todos los parámetros son correctos...
	if(!$error){
		# Leemos las etiquetas
		if(length $tags > 0){
			print $tags,"\n";
			@tags_array = split(/,/, $tags);
			foreach my $tag (@tags_array) {
		  		$tag = lc($tag);
				$tag =~ s/^\s+|\s+$//g
			}
		}
		# Extraemos los datos del fichero TSV
		($summary, $num_genes) = readTSV($tsv_file);
		# Lo almacenamos en MongoDB
		$mongodb->delete_one({ tax_id => $tax_id });
		$mongodb->insert_one({
			name => $name,
			tax_id => $tax_id,
			source => $source,
			tags => \@tags_array,
			date => DateTime->now,
			genes => $num_genes,
			summary => $summary
		});
		print "Model organism uploaded successfully !!!\n";
	}

} elsif($option eq 'list') {
	##########
	# Listar organismos modelos existentes en BD
	##########
	print "NAME <> TAX ID <> SOURCE <> DATE <> GENES <> TAGS\n";
	$cursor = $mongodb->find();
	while ($document = $cursor->next) {
	   	print $document->{'name'}." <> ";
		print $document->{'tax_id'}." <> ";
		print $document->{'source'}." <> ";
		print $document->{'date'}." <> ";
		print $document->{'genes'}." <> ";
		if($document->{'tags'}){
			print join(',', @{ $document->{'tags'} });
		}else{
			print "-";
		}
		print "\n";
	}
	print "\n";

} elsif($option eq 'delete') {
	##########
	# Eliminamos un organismo modelo según su TaxID
	##########
	# Comprobamos los parámetros
	if(length $tax_id == 0){
		print "Error: param -taxid required\n";
		$error = 1;
	}
	# Si todos los parámetros son correctos...
	if(!$error){
		# Eliminamos el organismos
		$mongodb->delete_one({ tax_id => $tax_id });
		print "$tax_id has been deleted\n";
	}

} elsif($option eq 'compare') {
	##########
	# Comparar un organismo contra un modelo
	##########
	# Comprobamos los parámetros
	if(length $tax_id == 0){
		print "Error: param -taxid required\n";
		$error = 1;
	}
	$error_file = existsFile($tsv_file);
	if(length $error_file > 0){
		print $error_file;
		$error = 1;
	}
	# Si todos los parámetros son correctos...
	if(!$error){
		# Cargamos el modelo a comparar
		$document = $mongodb->find_one({ tax_id => $tax_id });
		if(!exists $document->{'_id'}){
			print "Error: no organism has been found with that taxinomic identifier\n";
		}else{
			# Extraemos los datos del fichero TSV
			($summary, $num_genes) = readTSV($tsv_file);
			# Comparamos ambos organismos
			$distance = distance($summary, $num_genes, $document->{'summary'}, $document->{'genes'});
			print "Distance between your organism and $document->{'tax_id'}: $distance\n";
			# Comprobamos la existencia del fichero de resultados
			if(-e COMPARE_SUMARY_FILE) {
				# Generamos un JSON para enviar al fichero de resultados
				$json = JSON::MaybeXS->new->utf8;
				$json = $json->convert_blessed();
				$summary = $json->encode($summary);
				$json = $json->encode($document);
				# Relizamos una copia del fichero de resultados
				$replace = $document->{'tax_id'}.'_'.time().'.html';
				if(copy(COMPARE_SUMARY_FILE, $replace)){
					# Añadimos al nuevo fichero los datos en JSON
					$path = path($replace);
					$data = $path->slurp_utf8;
					$data =~ s/<MODEL_ORGANISM>/$json/g;
					$data =~ s/<MY_ORGANISM>/$summary/g;
					$data =~ s/<NUM_GENES>/$num_genes/g;
					$data =~ s/<DISTANCE>/$distance/g;
					$path->spew_utf8( $data );
					print "Generated comparation file «".$replace."»\n";
				}else{
					print "Error: file «".COMPARE_SUMARY_FILE."» could not be copied\n";
				}
			} else {
				print "Error: file «".COMPARE_SUMARY_FILE."» not found or empty\n";
			}
		}
	}

} elsif($option eq 'find') {
	##########
	# Comparar un organismo contra todos los modelos en
	# base de datos y listar los 5 más similares
	##########
	# Comprobamos los parámetros
	if(!defined $num_distances || $num_distances <= 0) {
		$num_distances = NUM_FIND_ORGANISM;
	}
	$error_file = existsFile($tsv_file);
	if(length $error_file > 0){
		print $error_file;
	}else{
		# Leemos las etiquetas
		if(length $tags > 0){
			@tags_array = split(/,/, $tags);
			foreach my $tag (@tags_array) {
				$tag = lc($tag);
				$tag =~ s/^\s+|\s+$//g
			}
		}
		# Extraemos los datos del fichero TSV
		($summary, $num_genes) = readTSV($tsv_file);
		# Cargamos de bbdd el conjunto de organismos modelo
		print "Getting models...\n";
		if(scalar @tags_array > 0){
			$cursor = $mongodb->find( {'tags' => {'$all' => \@tags_array}} );
		}else{
			$cursor = $mongodb->find();
		}
		# Calculamos las distancias con todos los organismos (hilos)
		print "Calculating distances...\n";
		while ($document = $cursor->next) {
			$num_hilos = threads->list();
			if($num_hilos > MAX_THREADS) { 
				$_->join() for threads->list();
			}
			threads->create( \&threadDistance );
		}
		# Esperamos a que terminen los hilos
		$_->join() for threads->list();
		# Ordenamos por distancia (menor a mayor)
		my %a; my %b;
		@distances = sort { $a->{distance} <=> $b->{distance} } @shared_distances;
		undef @shared_distances;
		# Reducimos y nos quedamos con las más pequeñas
		while(scalar @distances > $num_distances){
			splice @distances, scalar(@distances)-1, 1;
		}
		# Mostramos los resultados ordenados
		print "\nTAX ID <> ORGANISM <> DISTANCE\n";
		foreach my $distance (@distances) {
			print $distance->{'taxid'}." <> ".$distance->{'organism'}." <> ".$distance->{'distance'}."\n";
		}
		print "\n";
		# Comprobamos la existencia del fichero de resultados
		if(-e FIND_SUMARY_FILE) {
			# Generamos un JSON para enviar al fichero de resultados
			$json = JSON::MaybeXS->new->utf8;
			$json = $json->convert_blessed();
			foreach my $distance (@distances) {
				if(length $json_distances > 0){
					$json_distances .= ',';
				}
				$json_distances .= $json->encode($distance);
			}
			$json_distances = '['.$json_distances.']';
			$summary = $json->encode($summary);
			# Relizamos una copia del fichero de resultados
			$replace = 'find_'.time().'.html';
			if(copy(FIND_SUMARY_FILE, $replace)){
				# Añadimos al nuevo fichero los datos en JSON
				$path = path($replace);
				$data = $path->slurp_utf8;
				$data =~ s/<MODEL_ORGANISMS>/$json_distances/g;
				$data =~ s/<MY_ORGANISM>/$summary/g;
				$data =~ s/<NUM_GENES>/$num_genes/g;
				$path->spew_utf8( $data );
				print "Generated result file «".$replace."»\n";
			}else{
				print "Error: file «".FIND_SUMARY_FILE."» could not be copied\n";
			}
		} else {
			print "Error: file «".FIND_SUMARY_FILE."» not found or empty\n";
		}
	}

} else {
	print "Error: unknown option «$option»\n";
}

# Cerramos la conexión con MongoDB
$mongodb_connection->disconnect;

##############################
##############################

##########
## Definición de subrutinas del programa
##########

# Subrutina para calculo de distancias con hilos
sub threadDistance {
	lock @shared_distances;
	push @shared_distances, shared_clone({
		taxid => $document->{'tax_id'},
		source => $document->{'source'},
		organism => $document->{'name'},
		summary => $document->{'summary'},
		tags => $document->{'tags'},
		genes => $document->{'genes'},
		distance => distance($summary, $num_genes, $document->{'summary'}, $document->{'genes'})
	});
}

# Subrutina que conecta con MongoDB
sub mongoConnect {
	my $mongodb_connection_client = MongoDB::MongoClient->new(
		host => MONGODB_HOST,
		port => MONGODB_PORT
	);
	my $mongodb_connection_db = $mongodb_connection_client->get_database(MONGODB_DATABASE);
	my $mongodb_connection_coll = $mongodb_connection_db->get_collection(MONGODB_COLLECTION);
	return ($mongodb_connection_client, $mongodb_connection_coll);
}

# Subrutina que valida la existencia de un fichero
sub existsFile {
	my ($file) = @_;
	if(length $file == 0) {
		return "Error: param -file required\n";
	}
	if(!-e $file) {
		return "Error: file «$file» not found or empty\n";
	}
	return "";
}

# Subrutina que lee un fichero TSV proveniente de Sma3s y devuelve sus datos en un hash
sub readTSV {
	# Definición de variables locales de la subrutina
	my ($file) = @_;
	my $open_file;
	my $line;
	my @fields;
	my $header;
	my $num_genes;
	my $category = '';
	my %summary = ();
	# Lectura del fichero TSV
	print "Reading TSV file...\n";
	open($open_file, '<', $file) or die "Error: can't read file «$file» [$!]\n";
	while ($line = <$open_file>) {
		# Limpiar linea y separarla por tabuladores
		chomp $line;
		@fields = split(/\t/, $line);
		# Comprobar si es cabecera, elemento o linea vacia
		if(scalar @fields == 1 && length $fields[0] > 0){
			$header = substr($fields[0],1);
			$header =~ s/Category|"|categories//g;
			$header =~ s/_/ /g;
			$header =~ s/^\s+|\s+$//g;
			if($header eq 'GO Slim' || $header eq 'UniProt Keyword'){
				next;
			}
			if(not exists $summary{$header}){
				$summary{$header} = ();
			}
		}elsif(scalar @fields == 2 && $fields[1] > MIN_ANNOTATIONS){
			$summary{$header}{$fields[0]} = $fields[1];
		}elsif(scalar @fields == 3 && $fields[2] > MIN_ANNOTATIONS){
			$summary{$header}{$fields[0].': '.$fields[1]} = $fields[2];
		}
	}
	# Vaciar categorías vacías
	foreach $header (keys %summary) {
		if(!$summary{$header}){
			delete $summary{$header};
		}
	}
	# Obtención del número de genes
	if(exists $summary{'Annotation statistics'}){
		$num_genes = $summary{'Annotation statistics'}{'Number of query sequences:'};
		delete $summary{'Annotation statistics'};
	}else{
		$num_genes = $summary{'Annotation summary'}{'Number of query sequences:'};
		delete $summary{'Annotation summary'};
	}
	# Retorno del hash con los datos
	return (\%summary, $num_genes);
}

# Subrutina que lee un fichero CSV con los datos de una serie de organismos para su subida múltiple
# Formato del fichero: organism ; taxid ; source ; tags ; file
sub readCSVTable {
	# Definición de variables locales de la subrutina
	my ($file) = @_;
	my $open_file;
	my $line;
	my @fields;
	my $command;
	my $actual_path;
	my @organisms_path;
	# Obtenemos la ruta a los ficheros de organismos
    @organisms_path = fileparse($file);
	# Obtenemos la ruta al script actual
	$actual_path = Cwd::cwd();
	# Lectura del fichero TSV
	print "Reading CSV table file...\n";
	open($open_file, '<', $file) or die "Error: can't read file «$file» [$!]\n";
	while ($line = <$open_file>) {
		# Limpiar linea y separarla por punto-y-coma
		chomp $line;
		@fields = split(/;/, $line);
		# Creamos sentencia
        $command = "perl \"$actual_path/compatax.pl\" -option upload -name \"$fields[0]\" -file \"$organisms_path[1]$fields[4]\" -source \"$fields[2]\" -taxid $fields[1] -tags \"$fields[3]\"";
	    print $command."\n";
	    system($command);
	}
}

# Subrutina que calcula la distancia entre dos organismos.
# La distancia se calcula como el sumatorio del valor absoluto de la diferencia
# de cada uno de los contadores de anotaciones, independientemente de su categoría
sub distance {
	# Definición de variables locales de la subrutina
	my %org1 = %{(shift)};
	my $org1_genes = shift;
	my %org2 = %{(shift)};
	my $org2_genes = shift;
	my $header;
	my $annotation;
	my $reference_header;
	my $distance = 0;
	# Recorrido de las anotaciones del organismo 1
	foreach $header (keys %org1){
		my $reference_header = $org1{$header};
		foreach $annotation (keys %$reference_header){
			# Comprobamos si existe la anotación en el organismo 2
			if(exists $org2{$header} && exists $org2{$header}{$annotation}){
				# Aumentamos la distancia con el valor absoluto de la diferencia
				$distance += abs( ($org1{$header}{$annotation} / $org1_genes) - ($org2{$header}{$annotation} / $org2_genes) );
			}else{
				# Aumentamos la distancia con el valor de la anotacion
				$distance += ($org1{$header}{$annotation} / $org1_genes);
			}
		}
	}
	# Recorrido de las anotaciones del organismo 2
	foreach $header (keys %org2){
		my $reference_header = $org2{$header};
		foreach $annotation (keys %$reference_header){
			# Comprobamos si existe la anotación en el organismo 1
			if(!(exists $org1{$header} && exists $org1{$header}{$annotation})){
				# Aumentamos la distnaica con el valor de la anotacion
				$distance += ($org2{$header}{$annotation} / $org2_genes);
			}
		}
	}
	# Retorno del valor de distancia entre los organismos
	return $distance;
}

# Subrutina que muestra la ayuda
sub help {
	print "NAME\n\tCompatax\n\n";
	print "DESCRIPTION\n\tTaxonomic comparison tool of proteomes annotated by Sma3s v2\n\n";
	print "AUTHOR\n\tRoldan Ales F.J. ".'fran_roldan@outlook.com'."\n\n";
	print "USAGE\n\t# print a help message\n\tperl compatax.pl -h\n\n";
	print "\t# upload new proteome to database\n\t";
	print "perl compatax.pl -option upload -file summary.tsv -name Ecoli -source Uniprot -taxid 34634634 -tags bacteria,coli\n\n";
	print "\t# upload multiple proteomes to database\n\t";
	print "perl compatax.pl -option uploadmultiple -file table.tsv\n\n";
	print "\t# list model proteomes in database\n\t";
	print "perl compatax.pl -option list\n\n";
	print "\t# compare a annotated sma3s proteome against a model proteome stored in database (model id is 1234)\n\t";
	print "perl compatax.pl compare -file summary.tsv -taxid 1234\n\n";
	print "\t# look for the most similar model proteomes\n\t";
	print "perl compatax.pl find -file summary.tsv [-num 5] [-tags bacteria,procaryote]\n\n";
}
